# CrossRef2TS

The code in this project extracts articles marked as having or being translations
from the CrossRef DOI database and puts them in the Translate Science intermediary 
text format. The code does some automatic data cleaning. Manual data cleaning and adding
is done later with the text format data.

For more background information on Translate Science see our homepage:
https://wiki.translatescience.org
This page gives some more information on the CrossRef database and the translation data
therein:
https://wiki.translatescience.org/wiki/Translated_articles_in_the_CrossRef_database