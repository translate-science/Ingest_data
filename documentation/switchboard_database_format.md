# Translate Science Data sheets
## Design considerations
- Should be easy to revert data, like in Wikipedia, to make the tools as open as possible. So entries should have a date, so that the system can show the newest version (as default). (Someone reverting to an old version would copythis version with the current time and date.)
- The data sheets for the original and the translation should be similar, so we can query them with one set of functions. One could then also make one list, but having two sheets speeds up the search. (In the rare case that there is no well-defined original and translation, both should be listed in the sheet for originals and for translations.)
- We would like to set up the system in a way that the first version is quite simple, as well as setting up the system in a way that the final, more complete system, only adds, but does not change what was implemented in the initial simple system.
- **There could be translations where it is not allowed to publish them due to copy rights. Maybe that is a case we should also consider where translator can leave contact information so that interested people can email them for a private copy.**

## Proposal
The proposal below is for the database sheets to use and the variable names in them.

### Main sheet

| Field | Type | Required | Description |
|------|-------|----------|-------------|
| ID of original | String | yes | A database ID to find the original in other tables |
| ID of translation | String | yes | A database ID to find the translation in other tables |
| Date | Date (ISO) | yes | Time and Date the entry was made. In case an entry is reverted, the old entry gets the current date to make it the newest again, which is displayed. |
| type | String | no | The type of translation: partial, related, summary, ... | 
| ID of the comments | String | no | In future we would like to have some sort of forum per original article where people can discuss the translations. Theoretically this could be the same ID as the original, but the forum software may generate its own IDs that need to be stored. |

### Simple bibliography sheet
Information on the original in one sheet of this format and information on the translation on another sheet of this format.

| Field | Type | Required | Description |
|------|-------|----------|-------------|
| title | String | yes | Title of the work. Works can have multiple titles, this will be taken into account in the detailed sheet. |
| authorString | String | no | Ben's example sheet had articles with one author, I propose a string with the entire author list. In CrossRef authors are an array of the structure contributor with multiple entries. That is included in the detailed sheet as author-list. |
| publicationName | String | no | Name of the journal, CrossRef calls this the container-title. |
| volume | String | no | Volume of the issue the article is in. It is a string in CrossRef, I guess it is not always a pure number. |
| issue | String | no | Issue number of an article's journal. |
| pages | String | No | Pages numbers of an article within its journal. |
| publicationYear | String | No | Year in which the work was published for use in the reference. |
| language | string | no | ISO string indicating the language of the work. |
| DOI | String | no | Universal Resource Locator. |
| URL | String | no | Universal Resource Locator. Shall we make the type "URL", rather than on "String"? A URL should get more checks than a simple string. |
| UDC | String | no | Universal Decimal Classification of the topic of the work. | 
| comments | String | no | Internal comments. We will create something like a commenting system for users, but that needs to be implemented differently. | 

**Notes:**
- Ben had an entry abstract. We could put that in the simple sheet. Technically I feel it fits better in the detailed sheet for later, but it is quite important for findability, so maybe we do want to do have this in the first version.
- Ben, is the "translation author" in your system the same as the "original author"? Could we call the translator the person who made the translation? (At least internally?)
- The UDC would be double (the same for original and translation) or we could put it in the main sheet.

### Detailed bibliography sheet
This sheet should allow for advanced search using all typical bibliographic entries, should allow us to output the citation in various formats and should be compatible with the CrossRef system (likely takes all possible cases into account and makes data transfer easier). Authors with IDs (rather than just strings) should allow us to link people to other works by the same author.

CrossRef has a separate sheet for "references". This seems to repeat much of the full information, but has fields that would be used to create a printed reference list. Might be something for us to consider.

Status: preliminary. 

| Field | Type | Required | Description |
|-------|------|----------|-------------|
| publisher | String | Yes | Name of work's publisher. |
| title | Array of String | Yes | Work titles, including translated titles. |
| abstract | XML String | No | Abstract as a JSON string or a JATS XML snippet encoded into a JSON string. |
| DOI | String | Yes | DOI of the work. |
| URL | URL | Yes | URL form of the work's DOI. VV: Reuse for translations that only have an URL, but no DOI? |
| containerTitle | Array of String | No | Full titles of the containing work (usually a book or journal). |
| shortContainerTitle | Array of String | No | Abbreviated titles of the containing work. |
| groupTitle | String | No | Group title for posted content. |
| issue | String | No | Issue number of an article's journal. |
| volume | String | No | Volume number of an article's journal. |
| page | String | No | Pages numbers of an article within its journal. |
| firstPage | String | No | |
| articleNumber | String | No | |
| publishedPrint | [Partial Date](#partialDate) | No | Date on which the work was published in print. |
| publishedOnline | [Partial Date](#partialDate) | No | Date on which the work was published online. |
| subject | Array of String | No | Subject category names, a controlled vocabulary from Sci-Val. Available for most journal articles. |
| ISSN | Array of String | No | |
| issnType | Array of [ISSN with Type](#issnWithType) | No | List of ISSNs with ISSN type information. |
| ISBN | Array of String | No | |
| authorList | Array of [Contributor](#contributor) | No | Called author in CrossRef. |
| editor | Array of [Contributor](#contributor) | No | |
| chair | Array of [Contributor](#contributor) | No | |
| translator | Array of [Contributor](#contributor) | No | |
| alternativeID | Array of String | No | Other identifiers for the work provided by the depositing member (VV: Many from Wikidata). |
| unstructuredReference | String | No | |
| edition | String | No | |
| year | String | No | The year as used in a reference |
| sourceClass | String | no | Source of the translation record (TranslateScience, CrossRef, WikiData, etc.). |
| sourcePerson | Array of [Contributor](#contributor) | no | Name of the person who created the record or treat it like a contributor? |
| copyrightLicense | String | No | Useful? |

#### Not included, but options
| Field | Type | Required | Description |
|-------|------|----------|-------------|
| originalTitle | Array of String | No | Work titles in the work's original publication language |
| shortTitle | Array of String | No | Short or abbreviated work titles |
| subtitle | Array of String | No | Work subtitles, including original language and translated |
| archive | Array of String | No | |
| license | Array of [License](#license) | No | In CrossRef this is used to distinguish between the verison or record, the accepted manuscripts, ... |

### ISSN with Type

| Field | Type | Required | Description |
|-------|------|----------|-------------|
| value | String | Yes | |
| type | String | Yes | One of `eissn`, `pissn` or `lissn` |


### Contributor

| Field | Type | Required | Description |
|-------|------|----------|-------------|
| family | String | Yes | |
| given | String | No | |
| ORCID | URL | No | URL-form of an [ORCID](http://orcid.org) identifier |
| authenticatedOrcid | Boolean | No | If true, record owner asserts that the ORCID user completed ORCID OAuth authentication |
| affiliation | Array of [Affiliation](#affiliation) | No | |

VV: Add more IDs?  Add contact information (email, twitter, etc.?). Contact information would be important in case the translation cannot be published and people need to contact the translator for a private copy.

### Date

| Field | Type | Required | Description |
|-------|------|----------|-------------|
| dateParts | Array of Number | Yes | Contains an ordered array of `year`, `month`, `day of month`. Note that the field contains a nested array, e.g. `[ [ 2006, 5, 19 ] ]` to conform to citeproc JSON dates |
| timestamp | Number | Yes | Seconds since UNIX epoch |
| date-time | String | Yes | ISO 8601 date time |

### Partial Date

| Field | Type | Required | Description |
|-------|------|----------|-------------|
| date-parts | Array of Number | Yes | Contains an ordered array of `year`, `month`, `day of month`. Only `year` is required. Note that the field contains a nested array, e.g. `[ [ 2006, 5, 19 ] ]` to conform to citeproc JSON dates |
